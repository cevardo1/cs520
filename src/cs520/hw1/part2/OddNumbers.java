package cs520.hw1.part2;

import javax.swing.JOptionPane;

public class OddNumbers {
	
	private static int sumWithLoop (int n) {
	
		// Declare local variables for loop tracking, exit condition, and sum
		// Initialize to 0 and true
		// Double could be used here to handle larger numbers, but requirements started int
		int sum = 0;
		int i = 0;
		int count = 0;
		boolean looping = true;
		
		// Begin looping for a user defined amount of time
		while (looping) {
			
			// Check if iteration count equals user defined input, else begin calculation
			if (count == n) {
				looping =  false;
			} else { 
			// Condition to check for odd numbers	
				if (i % 2 != 0) {
					// Calculate current odd interation's square and add to final sum
					sum += Math.pow(i, 2);
					/*System.out.println("sum: "+sum);  DEBUGGING*/
	
					// Increment odd number counter
					count++;
					/*System.out.println("count: " + count); DEBUGGING*/			
				}			
			}
			
			// Increment iteration counter
			i++;
		}
		return sum;
	}
	
	
	private static int sumWithoutLoop(int n) {
		
		// Initialize numerator and denominator according to the given equation
		int numerator = (n*(2*n - 1))*((2*n+1));
		int denominator = 3;
		
		// Divide numerator and denominator to return the sum
		return numerator / denominator;
	}	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try {
			// Take user input, parse the output string into Integer wrapper class and cast to primitive int
			int inputNumber = Integer.parseInt(JOptionPane.showInputDialog("Enter an integer (>= 0):"));
			
			int result1;
			int result2;
			
		
			// Execute sumWithLoop and sumWithoutLoop with inputNumber and store results
			result1 = sumWithLoop(inputNumber);
			result2 = sumWithoutLoop(inputNumber);
			
			// Print results to console
			System.out.println("sumWithLoop: " + result1);
			System.out.println("sumWithoutLoop: " + result2);
			
			// Check if results of both methods have matching solutions.  Print feedback.
			if (result1 == result2) {
				System.out.println("Results are the same, my code is correct!");			
			} else {
				System.out.println("Results do not match! Fix your code!!!");
				System.out.println(result1 + " != " + result2 );
			}
			
			// Display message dialog with summary information for user
			JOptionPane.showMessageDialog(null, 
					"Requested Number (" + inputNumber + ")\n" +
					"Sum with loop (" + result1 + ")\n" +
					"Sum without loop (" + result2 + ")"
			);
		
		} catch (NumberFormatException e) {
			System.out.println("Invalid input.  Number must be an integer value.");
			System.out.println("Exiting...");
			
		} finally {
			// Exit and release resources, if any
			System.exit(0);
		}
		
	}







}

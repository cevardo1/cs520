/**
 * 
 */
package cs520.hw1.part1;

/**
 * @author chris
 *
 */
public enum Denomination {

	HUNDRED(100),
	TWENTY(20),
	TEN(10),
	FIVE(5),
	ONE(1);
	
	private final int value;
	
	private Denomination (int value) {
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}
	
	
}

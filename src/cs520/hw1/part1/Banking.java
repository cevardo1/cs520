/**
 * 
 */
package cs520.hw1.part1;

import javax.swing.JOptionPane;

/**
 * @author chris
 *
 */
public class Banking {
	
	
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Begin Banking application....");
		
		// Use try catch block to handle null pointer exceptions when user clicks Cancel on dialog boxes
		try {
			
			// Parse the input string to a Integer wrapper object and initialize the amount integer
			// Use int to handle potentially larger input values.  Likely not the best implementation.
			int amount = Integer.parseInt((JOptionPane.showInputDialog("Enter the amount: ")));
	
			// Null check input
			if (amount > -1) {
				
				// Print amount input to console
				System.out.println("Requested Amount: " + amount);
				
				// Set remainingAmount to zero
				int remainingAmount = amount;
				
				
				/* 
				 * Determine the number of times each denomination is divisible by the remainingAmount.
				 * Mod remainingAmount against the denomination size to find the remainder and storing
				 * the sum to remainingAmount variable
				*/
				int hundreds = remainingAmount / Denomination.HUNDRED.getValue();
				remainingAmount %= Denomination.HUNDRED.getValue();
				System.out.println("Hundreds = " + hundreds + ", Remaining Amount = " + remainingAmount);
				
				
				int twenties = remainingAmount / Denomination.TWENTY.getValue();
				remainingAmount %= Denomination.TWENTY.getValue();
				System.out.println("Twenties = " + twenties + ", Remaining Amount = " + remainingAmount);
				
				
				int tens = remainingAmount / Denomination.TEN.getValue();
				remainingAmount %= Denomination.TEN.getValue();
				System.out.println("Tens = " + tens + ", Remaining Amount = " + remainingAmount);
				
				
				int fives = remainingAmount / Denomination.FIVE.getValue();
				remainingAmount %= Denomination.FIVE.getValue();
				System.out.println("Fives = " + fives + ", Remaining Amount = " + remainingAmount);
				
				
				int ones = remainingAmount / Denomination.ONE.getValue();
				remainingAmount %= Denomination.ONE.getValue();
				System.out.println("Ones = " + ones + ", Remaining Amount = " + remainingAmount);
				
				
				// Create message pane to display the final results
				JOptionPane.showMessageDialog(null, 
						"Requested Amount: (" + amount + ")\n" + 
						"Hundreds: (" + hundreds + ")\n" +
						"Twenties: (" + twenties + ")\n" +
						"Tens: (" + tens + ")\n" +
						"Fives: (" + fives + ")\n" +
						"Ones: (" + ones + ")"
				);
			
			} else {
				// Print message to console if user inputs negative values
				System.out.println("Input must be a positive number. Banking operation cancelled.");
			}

			System.out.println("Exiting Banking application...");	
			
			
		} catch (Exception e) {
			// Catch exception when user clicks the cancel button
			System.out.println("User cancelled operation.  Exiting Banking application...");
			
		} finally {
			// Exit and release resources, if any
			System.exit(0);
		}

	}

}
